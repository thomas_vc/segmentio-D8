<?php

/**
 * @file
 * Contains \Drupal\segmentio\Form\SegmentioAdminSettingsForm.
 */

namespace Drupal\segmentio\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class SegmentioAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'segmentio_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable('segmentio.settings');
    // Get the custom fields their value (starting with "segmentio_").
    foreach ($form_state->getValues() as $key => $value) {
      if (strpos($key, "segmentio_") === 0) {
        $fieldValues[$key] = $value;
      }
    }

    // Clear cache when the privacy settings changed.
    if ($config->get('segmentio_privacy') <> $form_state->getValue("segmentio_privacy")) {
      drupal_flush_all_caches();
      drupal_set_message('All cache cleared.');
    }

    foreach ($fieldValues as $formField => $formFieldValue) {
      // Check if the custom field has an already declared space in the database.
        $config->set($formField, $formFieldValue);
    }

    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['segmentio.settings'];
  }

  public function buildForm(array $form_state, FormStateInterface $form_state) {
    $form['account'] = [
      '#type' => 'fieldset',
      '#title' => t('Basic Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    // Use 'segmentio_' in field names for custom config setting.
    $form['account']['segmentio_write_key'] = [
      '#title' => t('Write Key'),
      '#type' => 'textfield',
      '#default_value' => \Drupal::config('segmentio.settings')->get('segmentio_write_key'),
      '#size' => 200,
      '#maxlength' => 200,
      '#required' => TRUE,
      '#description' => t('This Write Key is unique to each Project you have configured in <a href="@segmentio">Segment.io</a>. To get a Write Key, <a href="@analyticsjs">register your Project with Segment.io</a>, or if you already have registered your site, go to your Segment.io Project Settings page.analyticsjs will use this write key to send data to your project.', [
        '@segmentio' => 'https://segment.io/login',
        '@analyticsjs' => 'https://segment.io/login',
      ]),
    ];

    // Advanced segmentio configurations.
    $form['advanced'] = [
      '#type' => 'fieldset',
      '#title' => t('Advance Configurations'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    // Privacy configurations.
    $form['advanced']['segmentio_privacy'] = [
      '#type' => 'checkbox',
      '#title' => t('Universal web tracking opt-out'),
      '#description' => t('Turned on will disable all tracking.'),
      '#default_value' => \Drupal::config('segmentio.settings')->get('segmentio_privacy'),
    ];

//    // Tracking settings
//    // @FIXME
//    // Could not extract the default value because it is either indeterminate, or
//    // not scalar. You'll need to provide a default value in
//    // config/install/segmentio.settings.yml and config/schema/segmentio.schema.yml.
//    $form['advanced']['segmentio_track'] = [
//      '#type' => 'checkboxes',
//      '#title' => t('Track Settings'),
//      '#options' => \Drupal::moduleHandler()->invokeAll('segmentio_info'),
//      '#default_value' => \Drupal::config('segmentio.settings')->get('segmentio_track'),
//      '#description' => t('Select which additional information should be tracked.'),
//    ];

    return parent::buildForm($form,  $form_state);
  }

}
