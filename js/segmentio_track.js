(function ($, Drupal) {
    Drupal.behaviors.segmentioTrack = {
        attach: function (context, settings) {
            $(context).find('body').once('segmentioTrack').each(function () {
                analytics.track(drupalSettings.segmentio.segmentioTrack.page, {

                });
            });
        }
    };
})(jQuery, Drupal);
