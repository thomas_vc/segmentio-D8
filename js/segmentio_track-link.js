(function ($, Drupal) {
    Drupal.behaviors.segmentioTrackLink = {
        attach: function (context, settings) {
            $(context).find('body').once('segmentioTrackLink').each(function () {

                var links = $('body').find('a');
                links.each(function() {
                    analytics.trackLink($(this), 'Clicked ' + $(this).text(), {
                        destination: $(this).attr('href')
                    });
                });
            });
        }
    }
})(jQuery, Drupal);