(function ($, Drupal) {
    Drupal.behaviors.segmentioIdentify = {
        attach: function (context, settings) {
            $(context).find('body').once('segmentioIdentify').each(function () {
                //for(key in drupalSettings.segmentio.segmentioIdentify){
                //    console.log(drupalSettings.segmentio.segmentioIdentify[key]);
                //}
                analytics.identify(drupalSettings.segmentio.segmentioIdentify.id, {
                    name: drupalSettings.segmentio.segmentioIdentify.name,
                    email: drupalSettings.segmentio.segmentioIdentify.email
                });
            });
        }
    };
})(jQuery, Drupal);