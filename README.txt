Introduction
------------
Simplistic Drupal 8 version using the segmentio JS documentation.
Written by Thomas Van Canegem (TvC). Sponsored by corecrew and e-HIMS.
segmentio JS (for install, tracking links, etc) are added as Drupal libraries. A library gets added to pages with hook_page_attachments.
There is also a php library for segmentio but I've chosen the JS implementation for easy HTML element manipulations and its also recommended.
You might want to alter certain custom logic in segmentio.module or the js files (see Customization below).

segmentio for Drupal is the easiest way to integrate segmentio analytics into
your Drupal site.

By installing segmentio's Drupal plugin you can add any analytics service to
your site without touching any code.

segmentio lets you send your analytics data to Google Analytics, Mixpanel,
KISSmetrics, Chartbeat, and more... without having to integrate with each
and every one, saving you time.

Once you're setup, you can swap and add new analytics services with the
click of a button!

Requirements
------------
SegmentIO User Account

Installation
------------
 * Install as usual, Copy the 'segmentio' module directory in to your Drupal,
usually it goes in sites/all/modules.
 * Drush installation : use drush dl segmentio

Configuration
-------------
* Configure user permissions in Administration » People » Permissions:
  - Administer segmentio
    Users with this permission will be able to update your segmentio settings

Customization
-------------
Logic for specific HTML element tracking is done in the JS files.
Logic for everything else is done in the segmentio.module file.

Maintainers
-----------
Thomas Van Canegem (TvC)